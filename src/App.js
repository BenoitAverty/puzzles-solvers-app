import React from 'react';
import './App.css';

import { ClassicSudokuSolver, ClassicSudokuSetter, TapaSolver } from "puzzles-solvers"

function App() {
    const [classicSudoku, setClassicSudoku] = React.useState(null);
    const [classicSudokuValidated, validateClassicSudoku] = React.useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          Solve anything !
        </p>
      </header>

        <section>
            <h1>Classic Sudoku</h1>
            {
                classicSudokuValidated
                    ? <ClassicSudokuSolver gridString={classicSudoku} />
                    : (
                        <>
                            <div><ClassicSudokuSetter onChange={setClassicSudoku} /></div>
                            <div><button onClick={validateClassicSudoku}>Solve it!</button></div>
                        </>
                    )
            }

        </section>
        <section>
            <h1>Tapa</h1>
            <TapaSolver
                rows={10}
                columns={10}
                gridString={`
                    _,_,_,_,_,_,_,_,_,_,
                    2,_,_,14,_,_,15,_,_,4,
                    _,_,_,_,_,_,_,_,_,_,
                    _,_,_,33,_,_,24,_,_,_,
                    11,_,_,_,_,_,_,_,_,_,
                    _,_,_,_,_,_,_,_,_,2,
                    _,_,_,22,_,_,13,_,_,_,
                    _,_,_,_,_,_,_,_,_,_,
                    3,_,_,5,_,_,5,_,_,3,
                    _,_,_,_,_,_,_,_,_,_
                `} />
        </section>
    </div>
  );
}

export default App;
